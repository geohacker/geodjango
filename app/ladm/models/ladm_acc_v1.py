from django.db import models
from django.contrib.gis.db import models
import uuid
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here


#Block 1, identifier = CC_
class Cc_barrio(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4()) 
    codigo = models.CharField(max_length=13,null=True, blank=True)
    nombre = models.CharField(max_length=100,null=True, blank=True)
    codigo_sector = models.CharField(max_length=9, null=True, blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "cc_barrio"
        managed = False
        
        
class Cc_centropoblado(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4())
    codigo = models.CharField(max_length=15,null=True, blank=True)
    nombre = models.CharField(max_length=100,null=True, blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "cc_centropoblado"
        managed = False

class Cc_corregimiento(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4())
    codigo =  models.CharField(max_length=20,null=True, blank=True)
    nombre =  models.CharField(max_length=100,null=True, blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "cc_corregimiento"
        managed = False

class Cc_localidadcomuna(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4())
    codigo = models.CharField(max_length=11,null=True, blank=True)
    nombre = models.CharField(max_length=100,null=True, blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "cc_localidadcomuna"
        managed = False

class Cc_manzana(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4())
    codigo = models.CharField(max_length=17,null=True, blank=True)
    codigo_barrio = models.CharField(max_length=13,null=True, blank=True)
    nombre = models.CharField(max_length=50, null=True, blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "cc_manzana"
        managed = False

class Cc_nomenclaturadomiciliaria(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,default=uuid.uuid4()) 
    predio_codigo = models.CharField(max_length=30,null=True, blank=True)
    valor_via_generadora = models.CharField(max_length=100,null=True, blank=True)
    letra_via_generadora = models.CharField(max_length=20,null=True, blank=True)
    numero_predio = models.CharField(max_length=255,null=True, blank=True)
    geometria = models.MultiLineStringField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "cc_nomenclaturadomiciliaria"
        managed = False

class Cc_nomenclaturavial(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4())
    tipo_via = models.ForeignKey('Cc_nomenclaturavial_tipo_via', models.DO_NOTHING, db_column='tipo_via',null=True) 
    numero_via = models.CharField(max_length=100,null=True, blank=True)
    geometria = models.MultiLineStringField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "cc_nomenclaturavial"
        managed = False

class Cc_nomenclaturavial_tipo_via(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024, null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "cc_nomenclaturavial_tipo_via"
        managed = False

class Cc_perimetrourbano(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4()) 
    codigo_departamento = models.CharField(max_length=2,null=True, blank=True)
    codigo_municipio = models.CharField(max_length=5,null=True, blank=True)
    tipo_avaluo = models.CharField(max_length=30,null=True, blank=True)
    nombre_geografico = models.CharField(max_length=255,null=True, blank=True)
    codigo_nombre = models.CharField(max_length=255,null=True, blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "cc_perimetrourbano"
        managed = False

class Cc_sectorrural(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4()) 
    codigo = models.CharField(max_length=9,null=True, blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "cc_sectorrural"
        managed = False

class Cc_sectorurbano(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4()) 
    codigo = models.CharField(max_length=9,null=True, blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "cc_sectorurbano"
        managed = False
    

class Cc_vereda(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4()) 
    codigo = models.CharField(max_length=17,null=True, blank=True)
    nombre = models.CharField(max_length=255,null=True, blank=True)
    codigo_sector = models.CharField(max_length=9,null=True, blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "cc_vereda"
        managed = False

#Block 2, identifier = Col_

class Col_baunitfuente(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4()) 
    fuente_gc_fuenteespacial = models.ForeignKey('Gc_fuenteespacial', models.DO_NOTHING, db_column='fuente_gc_fuenteespacial',null=True) 
    fuente_gc_fuenteadministrativa = models.ForeignKey('Gc_fuenteadministrativa', on_delete=models.CASCADE, db_column='fuente_gc_fuenteadministrativa',null=True) 
    baunit = models.ForeignKey('Gc_predio', on_delete=models.CASCADE, db_column='baunit',null=True) 
    class Meta:
        db_table = "col_baunitfuente"
        managed = False

class Col_cclfuente(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4())
    ccl = models.ForeignKey('Gc_lindero', models.DO_NOTHING, db_column='ccl',null=True) 
    fuente_espacial = models.ForeignKey('Gc_fuenteespacial', models.DO_NOTHING, db_column='fuente_espacial',null=True)   
    class Meta:
        db_table = "col_cclfuente"
        managed = False

class Col_estadodisponibilidadtipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024, null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "col_estadodisponibilidadtipo"
        managed = False

class Col_masccl(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4())
    ccl_mas = models.ForeignKey('Gc_lindero', models.DO_NOTHING, db_column='ccl_mas',null=True) 
    ue_mas_gc_servidumbretransito = models.ForeignKey('Gc_servidumbretransito', models.DO_NOTHING, db_column='ue_mas_gc_servidumbretransito',null=True) 
    ue_mas_gc_unidadconstruccion = models.ForeignKey('Gc_unidadconstruccion', models.DO_NOTHING, db_column='ue_mas_gc_unidadconstruccion',null=True) 
    ue_mas_gc_construccion = models.ForeignKey('Gc_construccion', models.DO_NOTHING, db_column='ue_mas_gc_construccion',null=True) 
    ue_mas_gc_terreno = models.ForeignKey('Gc_terreno', models.DO_NOTHING, db_column='ue_mas_gc_terreno',null=True) 

    class Meta:
        db_table = "col_masccl"
        managed = False

class Col_menosccl(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4()) 
    ccl_menos = models.ForeignKey('Gc_lindero', models.DO_NOTHING, db_column='ccl_menos',null=True) 
    ue_menos_gc_servidumbretransito = models.ForeignKey('Gc_servidumbretransito', models.DO_NOTHING, db_column='ue_menos_gc_servidumbretransito',null=True) 
    ue_menos_gc_unidadconstruccion = models.ForeignKey('Gc_unidadconstruccion', models.DO_NOTHING, db_column='ue_menos_gc_unidadconstruccion',null=True) 
    ue_menos_gc_construccion = models.ForeignKey('Gc_construccion', models.DO_NOTHING, db_column='ue_menos_gc_construccion',null=True) 
    ue_menos_gc_terreno = models.ForeignKey('Gc_terreno', models.DO_NOTHING, db_column='ue_menos_gc_terreno',null=True) 

    class Meta:
        db_table = "col_menosccl"
        managed = False

class Col_metodoproducciontipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True) 
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "col_metodoproducciontipo"
        managed = False

class Col_miembros(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4())
    interesado_gc_interesado = models.ForeignKey('Gc_interesado', on_delete=models.CASCADE, db_column='interesado_gc_interesado',null=True) 
    interesado_gc_agrupacioninteresados = models.ForeignKey('Gc_agrupacioninteresados', on_delete=models.CASCADE,related_name='Gc_agrupacioninteresados', db_column='interesado_gc_agrupacioninteresados',null=True) 
    agrupacion = models.ForeignKey('Gc_agrupacioninteresados', on_delete=models.CASCADE, related_name='agrupacion',  db_column='agrupacion',null=True) 
    class Meta:
        db_table = "col_miembros"
        managed = False

class Col_puntoccl(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4()) 
    punto_gc_puntocontrol = models.ForeignKey('Gc_puntocontrol', models.DO_NOTHING, db_column='punto_gc_puntocontrol',null=True) 
    punto_gc_puntolevantamiento = models.ForeignKey('Gc_puntolevantamiento', models.DO_NOTHING, db_column='punto_gc_puntolevantamiento',null=True) 
    punto_gc_puntolindero = models.ForeignKey('Gc_puntolindero', models.DO_NOTHING, db_column='punto_gc_puntolindero',null=True) 
    ccl = models.ForeignKey('Gc_lindero', models.DO_NOTHING, db_column='ccl',null=True) 
    class Meta:
        db_table = "col_puntoccl"
        managed = False

class Col_puntofuente(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4())
    fuente_espacial = models.ForeignKey('Gc_fuenteespacial', models.DO_NOTHING, db_column='fuente_espacial',null=True) 
    punto_gc_puntocontrol = models.ForeignKey('Gc_puntocontrol', models.DO_NOTHING, db_column='punto_gc_puntocontrol',null=True) 
    punto_gc_puntolevantamiento = models.ForeignKey('Gc_puntolevantamiento', models.DO_NOTHING, db_column='punto_gc_puntolevantamiento',null=True) 
    punto_gc_puntolindero = models.ForeignKey('Gc_puntolindero', models.DO_NOTHING, db_column='punto_gc_puntolindero',null=True) 
    class Meta:
        db_table = "col_puntofuente"
        managed = False

class Col_rrrfuente(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4())
    fuente_administrativa = models.ForeignKey('Gc_fuenteadministrativa', on_delete=models.CASCADE, db_column='fuente_administrativa',null=True) 
    rrr_gc_restriccion = models.ForeignKey('Gc_restriccion', models.DO_NOTHING, db_column='rrr_gc_restriccion',null=True) 
    rrr_gc_derecho = models.ForeignKey('Gc_derecho', on_delete=models.CASCADE, db_column='rrr_gc_derecho',null=True) 
    class Meta:
        db_table = "col_rrrfuente"
        managed = False

class Col_uebaunit(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4())
    ue_gc_servidumbretransito = models.ForeignKey('Gc_servidumbretransito', models.DO_NOTHING, db_column='ue_gc_servidumbretransito',null=True) 
    ue_gc_unidadconstruccion = models.ForeignKey('Gc_unidadconstruccion', models.DO_NOTHING, db_column='ue_gc_unidadconstruccion',null=True) 
    ue_gc_construccion = models.ForeignKey('Gc_construccion', models.DO_NOTHING, db_column='ue_gc_construccion',null=True) 
    ue_gc_terreno = models.ForeignKey('Gc_terreno',  models.DO_NOTHING, db_column='ue_gc_terreno',null=True) 
    baunit = models.ForeignKey('Gc_predio', on_delete=models.CASCADE, db_column='baunit',null=True) 
    class Meta:
        db_table = "col_uebaunit"
        managed = False

class Col_uefuente(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4()) 
    ue_gc_servidumbretransito = models.ForeignKey('Gc_servidumbretransito', models.DO_NOTHING, db_column='ue_gc_servidumbretransito',null=True) 
    ue_gc_unidadconstruccion = models.ForeignKey('Gc_unidadconstruccion', models.DO_NOTHING, db_column='ue_gc_unidadconstruccion',null=True) 
    ue_gc_construccion = models.ForeignKey('Gc_construccion', models.DO_NOTHING, db_column='ue_gc_construccion',null=True) 
    ue_gc_terreno = models.ForeignKey('Gc_terreno', models.DO_NOTHING, db_column='ue_gc_terreno',null=True) 
    fuente_espacial = models.ForeignKey('Gc_fuenteespacial', models.DO_NOTHING, db_column='fuente_espacial',null=True) 
    class Meta:
        db_table = "col_uefuente"
        managed = False

#Block 3, Identifier = ext

class Extarchivo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_seq = models.BigIntegerField(null=True, blank=True) 
    fecha_aceptacion = models.DateTimeField(null=True, blank=True)
    datos = models.CharField(max_length=255,null=True, blank=True)   
    extraccion = models.DateTimeField(null=True, blank=True)
    fecha_grabacion = models.DateTimeField(null=True, blank=True)
    fecha_entrega = models.DateTimeField(null=True, blank=True)
    gc_fuenteadministrtiva_ext_archivo_id = models.ForeignKey('Gc_fuenteadministrativa', models.DO_NOTHING, db_column='gc_fuenteadministrtiva_ext_archivo_id',null=True) 
    gc_fuenteespacial_ext_archivo_id = models.ForeignKey('Gc_fuenteespacial', models.DO_NOTHING,  db_column='gc_fuenteespacial_ext_archivo_id',null=True) 
    class Meta:
        db_table = "extarchivo"
        managed = False

class Extavaluo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_seq = models.BigIntegerField(null=True, blank=True) 
    avaluo_catastral = models.DecimalField(max_digits=16,decimal_places=1,null=True, blank=True)
    vigencia = models.DateField(null=True, blank=True)
    por_decreto = models.BooleanField(default=True)
    descripcion = models.CharField(max_length=255,null=True, blank=True) 
    gc_construccion_avaluo = models.ForeignKey('Gc_construccion', models.DO_NOTHING,null=True, blank=True, db_column='gc_construccion_avaluo') 
    gc_predio_avaluo = models.ForeignKey('Gc_predio', on_delete=models.CASCADE, db_column='gc_predio_avaluo',null=True) 
    gc_terreno_avaluo = models.ForeignKey('Gc_terreno', models.DO_NOTHING, db_column='gc_terreno_avaluo',null=True) 
    gc_servidumbretransito_avaluo = models.ForeignKey('Gc_servidumbretransito', models.DO_NOTHING, db_column='gc_servidumbretransito_avaluo',null=True) 
    gc_unidadconstruccion_avaluo = models.ForeignKey('Gc_unidadconstruccion', models.DO_NOTHING, db_column='gc_unidadconstruccion_avaluo',null=True) 
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(avaluo_catastral__gte=0 ),name='extavaluo_avaluo_catastral_check')]
        constraints += [models.CheckConstraint(check=models.Q(avaluo_catastral__lte=999999999999999),name='extavaluo_avaluo_catastral_check1')]
        db_table = "extavaluo"
        managed = False

class Extdireccion(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_seq  = models.BigIntegerField(null=True, blank=True)
    tipo_direccion = models.ForeignKey('Extdireccion_tipo_direccion', models.DO_NOTHING, db_column='tipo_direccion',null=True) 
    es_direccion_principal = models.BooleanField(null=True, blank=True)
    codigo_postal = models.CharField(max_length=255,null=True, blank=True)
    clase_via_principal = models.ForeignKey('Extdireccion_clase_via_principal', models.DO_NOTHING, db_column='clase_via_principal',null=True) 
    valor_via_principal = models.CharField(max_length=100,null=True, blank=True)
    letra_via_principal = models.CharField(max_length=20,null=True, blank=True)
    sector_ciudad = models.ForeignKey('Extdireccion_sector_ciudad', models.DO_NOTHING, db_column='sector_ciudad',null=True) 
    valor_via_generadora = models.CharField(max_length=100,null=True, blank=True)
    letra_via_generadora = models.CharField(max_length=20,null=True, blank=True)
    numero_predio = models.CharField(max_length=20,null=True, blank=True)
    sector_predio = models.ForeignKey('Extdireccion_sector_predio', models.DO_NOTHING, db_column='sector_predio',null=True) 
    complemento = models.CharField(max_length=255,null=True, blank=True)
    nombre_predio = models.CharField(max_length=255,null=True, blank=True)
    extinteresado_ext_direccion = models.ForeignKey('Extinteresado', models.DO_NOTHING,null=True) 
    gc_predio_direccion = models.ForeignKey('Gc_predio', on_delete=models.CASCADE, db_column='gc_predio_direccion',null=True) 
    localizacion = models.PointField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "extdireccion"
        managed = False

class Extdireccion_clase_via_principal(models.Model):    
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True) 
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "extdireccion_clase_via_principal"
        managed = False

class Extdireccion_sector_ciudad(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True) 
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "extdireccion_sector_ciudad"
        managed = False

class Extdireccion_sector_predio(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "extdireccion_sector_predio"
        managed = False

class Extdireccion_tipo_direccion(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True) 
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "extdireccion_tipo_direccion"
        managed = False

class Extinteresado(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_seq = models.BigIntegerField(null=True, blank=True) 
    nombre = models.CharField(max_length=255,null=True, blank=True)
    documento_escaneado = models.CharField(max_length=255,null=True, blank=True)
    gc_agrupacionintersdos_ext_pid = models.ForeignKey('Gc_agrupacioninteresados', models.DO_NOTHING, db_column='gc_agrupacionintersdos_ext_pid',null=True) 
    gc_interesado_ext_pid = models.ForeignKey('Gc_interesado', models.DO_NOTHING, db_column='gc_interesado_ext_pid',null=True) 
    class Meta:
        db_table = "extinteresado"
        managed = False

class Extlinderos(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    t_seq = models.BigIntegerField(null=True, blank=True) 
    norte =  models.TextField(null=True, blank=True)
    este = models.TextField(null=True, blank=True)
    sur = models.TextField(null=True, blank=True)
    oeste =  models.TextField(null=True, blank=True)
    gc_terreno_linderos = models.ForeignKey('Gc_terreno', models.DO_NOTHING, db_column='gc_terreno_linderos',null=True) 
    class Meta:
        db_table = "extlinderos"
        managed = False

#Block 4, Identifier = fraccion
class Fraccion(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_seq = models.BigIntegerField(null=True, blank=True) 
    denominador = models.IntegerField(null=True, blank=True)
    numerador = models.IntegerField(null=True, blank=True)
    col_miembros_participacion = models.ForeignKey('Col_miembros', models.DO_NOTHING, db_column='col_miembros_participacion',null=True) 
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(denominador__gte=0 ),name='fraccion_denominador_check')]
        constraints += [models.CheckConstraint(check=models.Q(denominador__lte=999999999),name='fraccion_denominador_check1')]
        constraints += [models.CheckConstraint(check=models.Q(numerador__gte=0 ),name='fraccion_numerador_check')]
        constraints += [models.CheckConstraint(check=models.Q(numerador__lte=999999999),name='fraccion_numerador_check1')]
        db_table = "fraccion"
        managed = False

#Block 5, Identifier = gc

class Gc_acuerdotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_acuerdotipo"
        managed = False

class Gc_agrupacioninteresados(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    tipo = models.ForeignKey('Gc_grupointeresadotipo', models.DO_NOTHING, db_column='tipo',null=True) 
    nombre = models.CharField(max_length=255,null=True, blank=True)
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True)  
    fin_vida_util_version = models.DateTimeField(null=True, blank=True)   
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    class Meta:
        db_table = "gc_agrupacioninteresados"
        managed = False

class Gc_categoriasuelotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024, null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_categoriasuelotipo"
        managed = False

class Gc_clasesuelotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True) 
    baseclass = models.CharField(max_length=1024, null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_clasesuelotipo"
        managed = False

class Gc_condicionprediotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True) 
    baseclass = models.CharField(max_length=1024, null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_condicionprediotipo"
        managed = False

class Gc_construccion(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4()) 
    identificador = models.CharField(max_length=20,null=True, blank=True)
    tipo_construccion = models.ForeignKey('Gc_construcciontipo', models.DO_NOTHING, db_column='tipo_construccion',null=True) 
    tipo_dominio = models.ForeignKey('Gc_dominioconstrucciontipo', models.DO_NOTHING, db_column='tipo_dominio',null=True) 
    numero_pisos = models.IntegerField(null=True, blank=True)
    numero_sotanos = models.IntegerField(null=True, blank=True)
    numero_mezanines = models.IntegerField(null=True, blank=True)
    numero_semisotanos = models.IntegerField(null=True, blank=True)
    anio_construccion = models.IntegerField(null=True, blank=True)
    altura = models.DecimalField(max_digits=6,decimal_places=2,null=True, blank=True)     
    codigo = models.CharField(max_length=30,null=True, blank=True)
    etiqueta = models.CharField(max_length=255,null=True, blank=True)
    area = models.DecimalField(max_digits=16,decimal_places=2,null=True, blank=True)  
    observacion = models.TextField(null=True, blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3, null=True, blank=True)
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True)
    fin_vida_util_version = models.DateTimeField(null=True, blank=True)  
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(altura__gte=0.0),name='gc_construccion_altura_check')]
        constraints += [models.CheckConstraint(check=models.Q(altura__lte=1000.0),name='gc_construccion_altura_check1')]
        constraints += [models.CheckConstraint(check=models.Q(anio_construccion__gte=1550),name='gc_construccion_anio_construccion_check')]
        constraints += [models.CheckConstraint(check=models.Q(anio_construccion__lte=2500),name='gc_construccion_anio_construccion_check1')]
        constraints += [models.CheckConstraint(check=models.Q(area__gte=0.0),name='gc_construccion_area_check')]
        constraints += [models.CheckConstraint(check=models.Q(area__lte=99999999999999.98),name='gc_construccion_area_check1')]
        constraints += [models.CheckConstraint(check=models.Q(numero_mezanines__gte=0 ),name='gc_construccion_numero_mezanines_check')]
        constraints += [models.CheckConstraint(check=models.Q(numero_mezanines__lte=99),name='gc_construccion_numero_mezanines_check1')]
        constraints += [models.CheckConstraint(check=models.Q(numero_pisos__gte=0 ),name='gc_construccion_numero_pisos_check')]
        constraints += [models.CheckConstraint(check=models.Q(numero_pisos__lte=300),name='gc_construccion_numero_pisos_check1')]
        constraints += [models.CheckConstraint(check=models.Q(numero_semisotanos__gte=0 ),name='gc_construccion_numero_semisotanos_check')]
        constraints += [models.CheckConstraint(check=models.Q(numero_semisotanos__lte=99),name='gc_construccion_numero_semisotanos_check1')]
        constraints += [models.CheckConstraint(check=models.Q(numero_sotanos__gte=0 ),name='gc_construccion_numero_sotanos_check')]
        constraints += [models.CheckConstraint(check=models.Q(numero_sotanos__lte=99),name='gc_construccion_numero_sotanos_check1')]
        db_table = "gc_construccion"
        managed = False

class Gc_construccionplantatipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_construccionplantatipo"
        managed = False

class Gc_construcciontipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True) 
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_construcciontipo"
        managed = False

class Gc_datosphcondominio(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    area_total_terreno = models.DecimalField(max_digits=16,decimal_places=2,null=True, blank=True)  
    area_total_terreno_privada = models.DecimalField(max_digits=16,decimal_places=2,null=True, blank=True)
    area_total_terreno_comun = models.DecimalField(max_digits=16,decimal_places=2,null=True, blank=True)
    area_total_construida = models.DecimalField(max_digits=16,decimal_places=2,null=True, blank=True)
    area_total_construida_privada = models.DecimalField(max_digits=16,decimal_places=2,null=True, blank=True)
    area_total_construida_comun = models.DecimalField(max_digits=16,decimal_places=2,null=True, blank=True)
    numero_torres = models.IntegerField(null=True, blank=True)
    total_unidades_privadas = models.IntegerField(null=True, blank=True)
    gc_predio = models.ForeignKey('Gc_predio', models.DO_NOTHING, db_column='gc_predio',null=True) 

    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(area_total_construida_privada__gte=0.0),name='gc_datosphcondominio_area_total_constrd_prvada_check')]
        constraints += [models.CheckConstraint(check=models.Q(area_total_construida_privada__lte=99999999999999.98),name='gc_datosphcondominio_area_total_constrd_prvada_check1')]
        constraints += [models.CheckConstraint(check=models.Q(area_total_construida_comun__gte=0.0),name='gc_datosphcondominio_area_total_construid_cmun_check')]
        constraints += [models.CheckConstraint(check=models.Q(area_total_construida_comun__lte=99999999999999.98),name='gc_datosphcondominio_area_total_construid_cmun_check1')]
        constraints += [models.CheckConstraint(check=models.Q(area_total_construida__gte=0.0 ),name='gc_datosphcondominio_area_total_construida_check')]
        constraints += [models.CheckConstraint(check=models.Q(area_total_construida__lte=99999999999999.98),name='gc_datosphcondominio_area_total_construida_check1')]
        constraints += [models.CheckConstraint(check=models.Q(area_total_terreno__gte=0.0 ),name='gc_datosphcondominio_area_total_terreno_check')]
        constraints += [models.CheckConstraint(check=models.Q(area_total_terreno__lte=99999999999999.98),name='gc_datosphcondominio_area_total_terreno_check1')]
        constraints += [models.CheckConstraint(check=models.Q(area_total_terreno_comun__gte=0.0 ),name='gc_datosphcondominio_area_total_terreno_comun_check')]
        constraints += [models.CheckConstraint(check=models.Q(area_total_terreno_comun__lte=99999999999999.98),name='gc_datosphcondominio_area_total_terreno_comun_check1')]
        constraints += [models.CheckConstraint(check=models.Q(area_total_terreno_privada__gte=0.0 ),name='gc_datosphcondominio_area_total_terreno_prvada_check')]
        constraints += [models.CheckConstraint(check=models.Q(area_total_terreno_privada__lte=99999999999999.98),name='gc_datosphcondominio_area_total_terreno_prvada_check1')]
        constraints += [models.CheckConstraint(check=models.Q(numero_torres__gte=0 ),name='gc_datosphcondominio_numero_torres_check')]
        constraints += [models.CheckConstraint(check=models.Q(numero_torres__lte=1000),name='gc_datosphcondominio_numero_torres_check1')]
        constraints += [models.CheckConstraint(check=models.Q(total_unidades_privadas__gte=0 ),name='gc_datosphcondominio_total_unidades_privadas_check')]
        constraints += [models.CheckConstraint(check=models.Q(total_unidades_privadas__lte=99999999),name='gc_datosphcondominio_total_unidades_privadas_check1')]
        db_table = "gc_datosphcondominio"
        managed = False

class Gc_derecho(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    tipo = models.ForeignKey('Gc_derechotipo', models.DO_NOTHING, db_column='tipo',null=True) 
    fraccion_derecho = models.DecimalField(max_digits=11,decimal_places=10,null=True, blank=True,default=1)  
    fecha_inicio_tenencia = models.DateField(null=True, blank=True)
    descripcion = models.CharField(max_length=255, null=True, blank=True)
    baunit = models.ForeignKey('Gc_predio', on_delete=models.CASCADE, db_column='baunit',null=True) 
    interesado_gc_interesado = models.ForeignKey('Gc_interesado', on_delete=models.CASCADE, db_column='interesado_gc_interesado',null=True) 
    interesado_gc_agrupacioninteresados = models.ForeignKey(Gc_agrupacioninteresados, on_delete=models.CASCADE, db_column='interesado_gc_agrupacioninteresados',null=True) 
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True)  
    fin_vida_util_version = models.DateTimeField(null=True, blank=True)
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(fraccion_derecho__gte=0.0),name='gc_derecho_fraccion_derecho_check')]
        constraints += [models.CheckConstraint(check=models.Q(fraccion_derecho__lte=1.0),name='gc_derecho_fraccion_derecho_check1')]
        db_table = "gc_derecho"
        managed = False

class Gc_derechotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_derechotipo"
        managed = False

class Gc_destinacioneconomicatipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024, null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_destinacioneconomicatipo"
        managed = False

class Gc_dominioconstrucciontipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_dominioconstrucciontipo"
        managed = False

class Gc_estadotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024, null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_estadotipo"
        managed = False

class Gc_fotoidentificaciontipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_fotoidentificaciontipo"
        managed = False

class Gc_fuenteadministrativa(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    tipo = models.ForeignKey('Gc_fuenteadministrativatipo', models.DO_NOTHING, db_column='tipo',null=True) 
    estado_disponibilidad = models.ForeignKey(Col_estadodisponibilidadtipo, models.DO_NOTHING, db_column='estado_disponibilidad',null=True) 
    numero_fuente = models.CharField(max_length=150,null=True, blank=True)
    fecha_documento_fuente = models.DateField(null=True, blank=True)
    ente_emisor = models.CharField(max_length=255,null=True, blank=True)
    descripcion = models.CharField(max_length=255, null=True, blank=True)
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    class Meta:
        db_table = "gc_fuenteadministrativa"
        managed = False

class Gc_fuenteadministrativatipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024, null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_fuenteadministrativatipo"
        managed = False

class Gc_fuenteespacial(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    tipo = models.ForeignKey('Gc_fuenteespacialtipo', models.DO_NOTHING, db_column='tipo',null=True) 
    metadato = models.TextField(null=True, blank=True)
    estado_disponibilidad = models.ForeignKey(Col_estadodisponibilidadtipo, models.DO_NOTHING, db_column='estado_disponibilidad',null=True) 
    numero_fuente = models.CharField(max_length=150,null=True, blank=True)
    fecha_documento_fuente = models.DateField(null=True, blank=True)
    ente_emisor = models.CharField(max_length=255,null=True, blank=True)
    descripcion = models.CharField(max_length=255,null=True, blank=True)
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    class Meta:
        db_table = "gc_fuenteespacial"
        managed = False

class Gc_fuenteespacialtipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024, null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_fuenteespacialtipo"
        managed = False

class Gc_grupoetnicotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_grupoetnicotipo"
        managed = False

class Gc_grupointeresadotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024, null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_grupointeresadotipo"
        managed = False

class Gc_sexotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)          
    class Meta:
        db_table = "gc_sexotipo"
        managed = False


class Gc_interesado(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    tipo = models.ForeignKey('Gc_interesadotipo', models.DO_NOTHING, db_column='tipo',null=True) 
    tipo_documento = models.ForeignKey('Gc_interesadodocumentotipo', models.DO_NOTHING, db_column='tipo_documento',null=True) 
    documento_identidad = models.CharField(max_length=50,null=True, blank=True)
    primer_nombre = models.CharField(max_length=100,null=True, blank=True)
    segundo_nombre = models.CharField(max_length=100,null=True, blank=True)
    primer_apellido = models.CharField(max_length=100,null=True, blank=True)
    segundo_apellido = models.CharField(max_length=100,null=True, blank=True)
    sexo = models.ForeignKey(Gc_sexotipo, models.DO_NOTHING, db_column='sexo',null=True) 
    grupo_etnico = models.ForeignKey(Gc_grupoetnicotipo, models.DO_NOTHING, db_column='grupo_etnico',null=True) 
    razon_social = models.CharField(max_length=255, null=True, blank=True)
    nombre = models.CharField(max_length=255,null=True, blank=True)
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True) 
    fin_vida_util_version = models.DateTimeField(null=True, blank=True)  
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    class Meta:
        db_table = "gc_interesado"
        managed = False

class Gc_interesadodocumentotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_interesadodocumentotipo"
        managed = False

class Gc_interesadotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_interesadotipo"
        managed = False

class Gc_lindero(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    longitud = models.DecimalField(max_digits=6,decimal_places=1,null=True, blank=True)   
    localizacion_textual = models.CharField(max_length=255,null=True, blank=True)
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True)  
    fin_vida_util_version = models.DateTimeField(null=True, blank=True)
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    geometria = models.LineStringField(srid=9377,dim=3, null=True, blank=True)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(longitud__gte=0.0),name='gc_lindero_longitud_check')]
        constraints += [models.CheckConstraint(check=models.Q(longitud__lte=10000.0),name='gc_lindero_longitud_check1')]
        db_table = "gc_lindero"
        managed = False

class Gc_predio(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True, default=uuid.uuid4())
    departamento = models.CharField(max_length=2)
    municipio = models.CharField(max_length=3)
    tiene_fmi = models.BooleanField(null=True, blank=True, default=True)
    codigo_orip = models.CharField(max_length=3,null=True, blank=True,default='156')
    matricula_inmobiliaria = models.CharField(max_length=80,null=True, blank=True)
    numero_predial = models.CharField(max_length=30,null=True, blank=True)
    numero_predial_anterior = models.CharField(max_length=20,null=True, blank=True)
    nupre = models.CharField(max_length=11,null=True, blank=True,default=None)
    interrelacionado = models.BooleanField(null=True, blank=True, default=False)
    nupre_fmi = models.BooleanField(null=True, blank=True,default=False)
    tipo = models.ForeignKey('Gc_prediotipo', models.DO_NOTHING, db_column='tipo',null=True) 
    condicion_predio = models.ForeignKey(Gc_condicionprediotipo, models.DO_NOTHING, db_column='condicion_predio',null=True) 
    destinacion_economica = models.ForeignKey(Gc_destinacioneconomicatipo, models.DO_NOTHING, db_column='destinacion_economica',null=True) 
    clase_suelo = models.ForeignKey(Gc_clasesuelotipo, models.DO_NOTHING, db_column='clase_suelo',null=True) 
    categoria_suelo = models.ForeignKey(Gc_categoriasuelotipo, models.DO_NOTHING, db_column='categoria_suelo',null=True) 
    area = models.DecimalField(max_digits=18,decimal_places=2,null=True, blank=True) 
    area_construida = models.DecimalField(max_digits=16,decimal_places=2, null=True, blank=True)
    estado = models.ForeignKey(Gc_estadotipo, models.DO_NOTHING, db_column='estado',null=True) 
    nombre = models.CharField(max_length=255,null=True, blank=True)
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True)  
    fin_vida_util_version = models.DateTimeField(null=True, blank=True)
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    rectificacion_area_linderos_registral = models.BooleanField(null=True, blank=True, default=False)
    sentencia_restitucion_tierras = models.BooleanField(null=True, blank=True, default=False)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(area__gte=0.0),name='gc_predio_area_check')]
        constraints += [models.CheckConstraint(check=models.Q(area__lte=1000000000000000),name='gc_predio_area_check1')]
        constraints += [models.CheckConstraint(check=models.Q(area_construida__gte=0.0),name='gc_predio_area_construida_check')]
        constraints += [models.CheckConstraint(check=models.Q(area_construida__lte=99999999999999.98),name='gc_predio_area_construida_check1 ')]
        db_table = "gc_predio"
        managed = False

# Temporal table
class Gc_predio_cancelado(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True, default=uuid.uuid4())
    departamento = models.CharField(max_length=2)
    municipio = models.CharField(max_length=3)
    numero_predial = models.CharField(max_length=30,null=True, blank=True)
    numero_predial_anterior = models.CharField(max_length=20,null=True, blank=True)
    codigo_homologado = models.CharField(max_length=11,null=True, blank=True)
    codigo_orip = models.CharField(max_length=3,null=True, blank=True,default='156')
    matricula_inmobiliaria = models.CharField(max_length=80,null=True, blank=True)
    tramite = models.CharField(max_length=50,null=True, blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = "gc_predio_cancelado"
        managed = False

# Temporal table
class Gc_predio_derivado(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True, default=uuid.uuid4())
    numero_predial = models.CharField(max_length=30,null=True, blank=True)
    predio_matriz = models.ForeignKey(Gc_predio_cancelado, models.DO_NOTHING,related_name='predio_matriz', db_column='predio_matriz',null=True) 
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = "gc_predio_derivado"
        managed = False

class Gc_prediocopropiedad(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    predio = models.ForeignKey(Gc_predio, models.DO_NOTHING,related_name='predio', db_column='predio',null=True) 
    copropiedad = models.ForeignKey(Gc_predio, models.DO_NOTHING,related_name='copropiedad', db_column='copropiedad',null=True) 
    coeficiente = models.DecimalField(max_digits=11,decimal_places=10,null=True, blank=True)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(coeficiente__gte=0.0),name='gc_prediocopropiedad_coeficiente_check')]
        constraints += [models.CheckConstraint(check=models.Q(coeficiente__lte=1.0),name='gc_prediocopropiedad_coeficiente_check1')]
        db_table = "gc_prediocopropiedad"
        managed = False

class Gc_prediotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024, null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_prediotipo"
        managed = False

class Gc_puntocontrol(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True,blank=True, default=uuid.uuid4())
    punto_tipo = models.ForeignKey('Gc_puntotipo', models.DO_NOTHING, db_column='punto_tipo',null=True) 
    tipo_punto_control = models.ForeignKey('Gc_puntocontroltipo', models.DO_NOTHING, db_column='tipo_punto_control',null=True) 
    exactitud_horizontal = models.DecimalField(max_digits=5,decimal_places=3,null=True, blank=True)
    exactitud_vertical = models.DecimalField(max_digits=5,decimal_places=3,null=True, blank=True)
    id_punto = models.CharField(max_length=255)
    metodo_produccion = models.ForeignKey(Col_metodoproducciontipo, models.DO_NOTHING, db_column='metodo_produccion',null=True) 
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True)  
    fin_vida_util_version = models.DateTimeField(null=True, blank=True)
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    geometria = models.PointField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(exactitud_horizontal__gte=0.0),name='gc_puntocontrol_exactitud_horizontal_check')]
        constraints += [models.CheckConstraint(check=models.Q(exactitud_horizontal__lte=10.0),name='gc_puntocontrol_exactitud_horizontal_check1')]
        constraints += [models.CheckConstraint(check=models.Q(exactitud_vertical__gte=0.0),name='gc_puntocontrol_exactitud_vertical_check')]
        constraints += [models.CheckConstraint(check=models.Q(exactitud_vertical__lte=10.0),name='gc_puntocontrol_exactitud_vertical_check1 ')]
        db_table = "gc_puntocontrol"
        managed = False

class Gc_puntocontroltipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_puntocontroltipo"
        managed = False

class Gc_puntolevantamiento(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    punto_tipo = models.ForeignKey('Gc_puntotipo', models.DO_NOTHING, db_column='punto_tipo',null=True) 
    tipo_punto_levantamiento = models.ForeignKey('Gc_puntolevtipo', models.DO_NOTHING, db_column='tipo_punto_levantamiento',null=True) 
    fotoidentificacion = models.ForeignKey(Gc_fotoidentificaciontipo, models.DO_NOTHING, db_column='fotoidentificacion',null=True) 
    exactitud_horizontal = models.DecimalField(max_digits=5,decimal_places=3,null=True, blank=True)
    exactitud_vertical = models.DecimalField(max_digits=5,decimal_places=3,null=True, blank=True)
    id_punto = models.CharField(max_length=255)
    metodo_produccion = models.ForeignKey(Col_metodoproducciontipo, models.DO_NOTHING, db_column='metodo_produccion',null=True) 
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True)  
    fin_vida_util_version = models.DateTimeField(null=True, blank=True)
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    geometria = models.PointField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(exactitud_horizontal__gte=0.0),name='gc_puntolevantamiento_exactitud_horizontal_check')]
        constraints += [models.CheckConstraint(check=models.Q(exactitud_horizontal__lte=10.0),name='gc_puntolevantamiento_exactitud_horizontal_check1')]
        constraints += [models.CheckConstraint(check=models.Q(exactitud_vertical__gte=0.0),name='gc_puntolevantamiento_exactitud_vertical_check')]
        constraints += [models.CheckConstraint(check=models.Q(exactitud_vertical__lte=10.0),name='gc_puntolevantamiento_exactitud_vertical_check1')]
        db_table = "gc_puntolevantamiento"
        managed = False

class Gc_puntolevtipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_puntolevtipo"
        managed = False

class Gc_puntolindero(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    punto_tipo = models.ForeignKey('Gc_puntotipo', models.DO_NOTHING, db_column='punto_tipo',null=True) 
    acuerdo = models.ForeignKey(Gc_acuerdotipo, models.DO_NOTHING, db_column='acuerdo',null=True) 
    fotoidentificacion = models.ForeignKey(Gc_fotoidentificaciontipo, models.DO_NOTHING, db_column='fotoidentificacion',null=True) 
    exactitud_horizontal = models.DecimalField(max_digits=5,decimal_places=3,null=True, blank=True)
    exactitud_vertical = models.DecimalField(max_digits=5,decimal_places=3,null=True, blank=True)
    id_punto = models.CharField(max_length=255,null=True, blank=True)
    metodo_produccion = models.ForeignKey(Col_metodoproducciontipo, models.DO_NOTHING, db_column='metodo_produccion',null=True) 
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True)  
    fin_vida_util_version = models.DateTimeField(null=True, blank=True) 
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    geometria = models.PointField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(exactitud_horizontal__gte=0.0),name='gc_puntolindero_exactitud_horizontal_check')]
        constraints += [models.CheckConstraint(check=models.Q(exactitud_horizontal__lte=10.0),name='gc_puntolindero_exactitud_horizontal_check1')]
        constraints += [models.CheckConstraint(check=models.Q(exactitud_vertical__gte=0.0),name='gc_puntolindero_exactitud_vertical_check')]
        constraints += [models.CheckConstraint(check=models.Q(exactitud_vertical__lte=10.0),name='gc_puntolindero_exactitud_vertical_check1')]
        db_table = "gc_puntolindero"
        managed = False

class Gc_puntotipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_puntotipo"
        managed = False

class Gc_restriccion(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    tipo = models.ForeignKey('Gc_restricciontipo', models.DO_NOTHING, db_column='tipo',null=True) 
    descripcion = models.CharField(max_length=255,null=True, blank=True)
    baunit = models.ForeignKey(Gc_predio, models.DO_NOTHING, db_column='baunit',null=True) 
    interesado_gc_interesado = models.ForeignKey(Gc_interesado, models.DO_NOTHING, db_column='interesado_gc_interesado',null=True) 
    interesado_gc_agrupacioninteresados = models.ForeignKey(Gc_agrupacioninteresados, models.DO_NOTHING, db_column='interesado_gc_agrupacioninteresados',null=True) 
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True)  
    fin_vida_util_version = models.DateTimeField(null=True, blank=True)  
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    class Meta:
        db_table = "gc_restriccion"
        managed = False

class Gc_restricciontipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_restricciontipo"
        managed = False

class Gc_servidumbretransito(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    codigo = models.CharField(max_length=30,null=True, blank=True)
    etiqueta = models.CharField(max_length=255,null=True, blank=True)
    area = models.DecimalField(max_digits=16,decimal_places=2,null=True, blank=True)
    observacion = models.TextField(null=True, blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3,null=True, blank=True)
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True)  
    fin_vida_util_version = models.DateTimeField(null=True, blank=True) 
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(area__gte=0.0),name='gc_servidumbretransito_area_check')]
        constraints += [models.CheckConstraint(check=models.Q(area__lte=99999999999999.98),name='gc_servidumbretransito_area_check1')]
        db_table = "gc_servidumbretransito"
        managed = False

class Gc_terreno(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    geometria = models.MultiPolygonField(srid=9377,dim=3,null=True, blank=True)
    codigo = models.CharField(max_length=30,null=True, blank=True)
    etiqueta = models.CharField(max_length=255,null=True, blank=True)
    area = models.DecimalField(max_digits=16,decimal_places=2,null=True, blank=True)
    observacion = models.TextField(null=True, blank=True)
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True)  
    fin_vida_util_version = models.DateTimeField(null=True, blank=True) 
    espacio_de_nombres = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(area__gte=0.0),name='gc_terreno_area_check')]
        constraints += [models.CheckConstraint(check=models.Q(area__lte=99999999999999.98),name='gc_terreno_area_check1')]
        db_table = "gc_terreno"
        managed = False

class Gc_unidadconstruccion(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.UUIDField(null=True, blank=True,default=uuid.uuid4())
    identificador = models.CharField(max_length=20)
    tipo_construccion = models.ForeignKey(Gc_construcciontipo, models.DO_NOTHING, db_column='tipo_construccion',null=True) 
    tipo_dominio = models.ForeignKey(Gc_dominioconstrucciontipo, models.DO_NOTHING, db_column='tipo_dominio',null=True) 
    tipo_unidad_construccion = models.ForeignKey('Gc_unidadconstrucciontipo', models.DO_NOTHING, db_column='tipo_unidad_construccion',null=True) 
    tipo_planta = models.ForeignKey(Gc_construccionplantatipo, models.DO_NOTHING, db_column='tipo_planta',null=True) 
    planta_ubicacion = models.IntegerField(null=True, blank=True)
    total_habitaciones = models.IntegerField(null=True, blank=True)
    total_banios = models.IntegerField(null=True, blank=True)
    total_locales = models.IntegerField(null=True, blank=True)
    uso = models.ForeignKey('Gc_usouconstipo', models.DO_NOTHING, db_column='uso',null=True) 
    anio_construccion = models.IntegerField(null=True, blank=True)
    area_privada_construida = models.DecimalField(max_digits=16,decimal_places=2,null=True, blank=True)
    altura = models.DecimalField(max_digits=6,decimal_places=2,null=True, blank=True)
    puntaje = models.IntegerField(null=True, blank=True)
    gc_construccion = models.ForeignKey(Gc_construccion, models.DO_NOTHING, db_column='gc_construccion',null=True) 
    codigo = models.CharField(max_length=30,null=True, blank=True)
    etiqueta = models.CharField(max_length=255,null=True, blank=True)
    area = models.DecimalField(max_digits=16,decimal_places=2,null=True, blank=True)
    observacion = models.TextField(null=True, blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3, null=True, blank=True)
    comienzo_vida_util_version = models.DateTimeField(null=True, blank=True)  
    fin_vida_util_version = models.DateTimeField(null=True, blank=True) 
    espacio_de_nombres  = models.CharField(max_length=255,null=True, blank=True)
    local_id = models.CharField(max_length=255,null=True, blank=True)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(altura__gte=0.0),name='gc_unidadconstruccion_altura_check')]
        constraints += [models.CheckConstraint(check=models.Q(altura__lte=1000.0),name='gc_unidadconstruccion_altura_check1')]
        constraints += [models.CheckConstraint(check=models.Q(anio_construccion__gte=1512),name='gc_unidadconstruccion_anio_construccion_check')]
        constraints += [models.CheckConstraint(check=models.Q(anio_construccion__lte=2500),name='gc_unidadconstruccion_anio_construccion_check1')]
        constraints += [models.CheckConstraint(check=models.Q(area__gte=0.0),name='gc_unidadconstruccion_area_check')]
        constraints += [models.CheckConstraint(check=models.Q(area__lte=99999999999999.98),name='gc_unidadconstruccion_area_check1')]
        constraints += [models.CheckConstraint(check=models.Q(area_privada_construida__gte=0.0),name='gc_unidadconstruccion_area_privada_construida_check')]
        constraints += [models.CheckConstraint(check=models.Q(area_privada_construida__lte=99999999999999.98),name='gc_unidadconstruccion_area_privada_construida_check1')]
        constraints += [models.CheckConstraint(check=models.Q(planta_ubicacion__gte=0),name='gc_unidadconstruccion_planta_ubicacion_check')]
        constraints += [models.CheckConstraint(check=models.Q(planta_ubicacion__lte=500),name='gc_unidadconstruccion_planta_ubicacion_check1')]
        constraints += [models.CheckConstraint(check=models.Q(puntaje__gte=0),name='gc_unidadconstruccion_puntaje_check')]
        constraints += [models.CheckConstraint(check=models.Q(puntaje__lte=200),name='gc_unidadconstruccion_puntaje_check1')]
        constraints += [models.CheckConstraint(check=models.Q(total_banios__gte=0),name='gc_unidadconstruccion_total_banios_check')]
        constraints += [models.CheckConstraint(check=models.Q(total_banios__lte=999999),name='gc_unidadconstruccion_total_banios_check1')]
        constraints += [models.CheckConstraint(check=models.Q(total_habitaciones__gte=0),name='gc_unidadconstruccion_total_habitaciones_check')]
        constraints += [models.CheckConstraint(check=models.Q(total_habitaciones__lte=999999),name='gc_unidadconstruccion_total_habitaciones_check1')]
        constraints += [models.CheckConstraint(check=models.Q(total_locales__gte=0),name='gc_unidadconstruccion_total_locales_check')]
        constraints += [models.CheckConstraint(check=models.Q(total_locales__lte=999999),name='gc_unidadconstruccion_total_locales_check1')]
        db_table = "gc_unidadconstruccion"
        managed = False

class Gc_unidadconstrucciontipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_unidadconstrucciontipo"
        managed = False

class Gc_usouconstipo(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    thisclass = models.CharField(max_length=1024,null=True, blank=True)
    baseclass = models.CharField(max_length=1024, null=True, blank=True)
    itfcode = models.IntegerField(null=True, blank=True)
    ilicode = models.CharField(max_length=1024,null=True, blank=True)
    seq = models.IntegerField(null=True, blank=True)
    inactive = models.BooleanField(null=True, blank=True)
    dispname = models.CharField(max_length=250,null=True, blank=True)
    description = models.CharField(max_length=1024,null=True, blank=True)
    class Meta:
        db_table = "gc_usouconstipo"
        managed = False

#Block 6, Identifie = gm_

class Gm_multicurve3d(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_seq = models.BigIntegerField(null=True, blank=True)
    class Meta: 
        db_table = "gm_multicurve3d"
        managed = False

class Gm_curve3dlistvalue(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_seq = models.BigIntegerField(null=True, blank=True)
    gm_multicurve3d_geometry = models.ForeignKey(Gm_multicurve3d, models.DO_NOTHING, db_column='gm_multicurve3d_geometry',null=True) 
    avalue = models.LineStringField(srid=9377,dim=3,null=True, blank=True)
    class Meta: 
        db_table = "gm_curve3dlistvalue"
        managed = False

class Gm_multisurface3d(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_seq = models.BigIntegerField(null=True, blank=True)
    class Meta: 
        db_table = "gm_multisurface3d"
        managed = False

class Gm_surface3dlistvalue(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_seq = models.BigIntegerField(null=True, blank=True)
    gm_multisurface3d_geometry = models.ForeignKey(Gm_multisurface3d, models.DO_NOTHING, db_column='gm_multisurface3d_geometry',null=True) 
    avalue = models.PolygonField(srid=9377,dim=3,null=True, blank=True)
    class Meta: 
        db_table = "gm_surface3dlistvalue"
        managed = False

#Block 7, Identifier = imagen

class Imagen(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_seq = models.BigIntegerField(null=True, blank=True)
    uri = models.CharField(max_length=255,null=True, blank=True)
    extinteresado_huella_dactilar = models.ForeignKey(Extinteresado, models.DO_NOTHING, related_name='extinteresado_huella_dactilar', db_column='extinteresado_huella_dactilar',null=True) 
    extinteresado_fotografia = models.ForeignKey(Extinteresado, models.DO_NOTHING,related_name='extinteresado_fotografia', db_column='extinteresado_fotografia',null=True) 
    extinteresado_firma = models.ForeignKey(Extinteresado, models.DO_NOTHING, related_name='extinteresado_firma',db_column='extinteresado_firma',null=True) 
    class Meta: 
        db_table = "imagen"
        managed = False

#Block 8, Identifier = t_ili2db

class T_ili2db_attrname(models.Model):
    iliname = models.CharField(max_length=1024,null=True, blank=True)  
    sqlname = models.CharField(max_length=1024,null=True, blank=True)  
    colowner = models.CharField(max_length=1024,null=True, blank=True)
    target = models.CharField(max_length=1024,null=True, blank=True)
    class Meta: 
        db_table = "t_ili2db_attrname"
        managed = False

class T_ili2db_basket(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    dataset = models.ForeignKey('T_ili2db_dataset', models.DO_NOTHING, db_column='dataset',null=True) 
    topic = models.CharField(max_length=200,null=True, blank=True)
    t_ili_tid = models.CharField(max_length=200,null=True, blank=True,default=uuid.uuid4())
    attachmentkey = models.CharField(max_length=200,null=True, blank=True)
    domains = models.CharField(max_length=1024,null=True, blank=True)
    class Meta: 
        db_table = "t_ili2db_basket"
        managed = False

class T_ili2db_classname(models.Model):
    iliname = models.CharField(max_length=1024,null=True, blank=True)
    sqlname = models.CharField(max_length=1024,null=True, blank=True)  
    class Meta: 
        db_table = "t_ili2db_classname"
        managed = False


class T_ili2db_column_prop(models.Model):
    tablename = models.CharField(max_length=255,null=True, blank=True) 
    subtype = models.CharField(max_length=255,null=True, blank=True)
    columnname = models.CharField(max_length=255,null=True, blank=True)
    tag = models.CharField(max_length=1024,null=True, blank=True)
    setting = models.CharField(max_length=1024,null=True, blank=True)
    
    class Meta: 
        db_table = "t_ili2db_column_prop"
        managed = False

class T_ili2db_dataset(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    datasetname = models.CharField(max_length=200,null=True, blank=True)
    class Meta: 
        db_table = "t_ili2db_dataset"
        managed = False

class T_ili2db_inheritance(models.Model):
    thisclass = models.CharField(primary_key=True,max_length=1024)
    baseclass = models.CharField(max_length=1024,null=True, blank=True)
    class Meta: 
        db_table = "t_ili2db_inheritance"
        managed = False

class T_ili2db_meta_attrs(models.Model):
    ilielement = models.CharField(max_length=255,null=True, blank=True)
    attr_name = models.CharField(max_length=1024,null=True, blank=True)
    attr_value = models.CharField(max_length=1024,null=True, blank=True)
    class Meta: 
        db_table = "t_ili2db_meta_attrs"
        managed = False

class T_ili2db_model(models.Model):
    filename = models.CharField(max_length=250,null=True, blank=True)
    iliversion = models.CharField(max_length=3,null=True, blank=True)   
    modelname = models.TextField(null=True, blank=True)  
    content = models.TextField(null=True, blank=True)
    importdate = models.DateTimeField(null=True, blank=True)
    class Meta: 
        db_table = "t_ili2db_model"
        managed = False

class T_ili2db_settings(models.Model):
    tag = models.CharField(primary_key=True,max_length=60)   
    setting = models.CharField(max_length=1024,null=True, blank=True)
    class Meta: 
        db_table = "t_ili2db_settings"
        managed = False


class T_ili2db_table_prop(models.Model):
    tablename = models.CharField(max_length=255,null=True, blank=True)
    tag = models.CharField(max_length=1024,null=True, blank=True)
    setting = models.CharField(max_length=1024,null=True, blank=True)
    class Meta: 
        db_table = "t_ili2db_table_prop"
        managed = False

class T_ili2db_trafo(models.Model):
    iliname = models.CharField(max_length=1024,null=True, blank=True)
    tag = models.CharField(max_length=1024,null=True, blank=True)
    setting = models.CharField(max_length=1024,null=True, blank=True)
    class Meta: 
        db_table = "t_ili2db_trafo"
        managed = False

#Block 9, Identifier = zhf,zhg, tv:construccion
#Add new tables zhf_rural, zhf_urbana, zhg_rural, zhg_urbana and tv_construccion to models django.
class Zhf_urbana(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True,blank=True,default=uuid.uuid4())
    codigo = models.CharField(max_length=7,null=True,blank=True)
    codigo_zona_fisica = models.CharField(max_length=4, null=True,blank=True)
    topografia = models.CharField(max_length=255, null=True,blank=True)
    influencia_vial = models.CharField(max_length=255, null=True, blank=True)
    servicio_publico = models.CharField(max_length=255, null=True,blank=True)
    uso_actual_suelo = models.CharField(max_length=255, null=True,blank=True)
    norma_uso_suelo = models.CharField(max_length=255, null=True, blank=True)
    tipificacion_construccion = models.CharField(max_length=255, null=True,blank=True)
    vigencia = models.DateField(null=True,blank=True)
    geometria = models.MultiPolygonField(srid=9377, dim=3, null=True,blank=True)
    class Meta: 
        db_table = "zhf_urbana"
        managed = False

class Zhf_rural(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200, null=True, blank=True,default=uuid.uuid4())
    codigo = models.CharField(max_length=7, null=True,blank=True)
    codigo_zona_fisica = models.CharField(max_length=4, null=True,blank=True)
    area_homogenea_tierra = models.CharField(max_length=255, null=True,blank=True)
    disponibilidad_agua = models.CharField(max_length=255,null=True,blank=True)
    influencia_vial = models.CharField(max_length=255, null=True,blank=True)
    uso_actual_suelo = models.CharField(max_length=255, null=True,blank=True)
    norma_uso_suelo = models.CharField(max_length=255, null=True,blank=True)
    vigencia = models.DateField(null=True,blank=True)
    geometria = models.MultiPolygonField(srid=9377,dim=3,null=True, blank=True)
    class Meta:
        db_table = "zhf_rural"
        managed = False

class Tv_construccion(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200, null=True, blank=True,default=uuid.uuid4())
    codigo = models.CharField(max_length=5, null=True, blank=True)
    tipo_avaluo = models.CharField(max_length=2, null=True,  blank=True)
    uso = models.ForeignKey('Gc_usouconstipo', models.DO_NOTHING, db_column='uso',null=True)
    puntaje = models.IntegerField(null=True, blank=True)
    valor_metro = models.DecimalField(max_digits=16,decimal_places=1,null=True, blank=True)
    vigencia = models.DateField(null=True, blank=True)
    agropecuario = models.BooleanField(null=True,blank=True)
    class Meta: 
        constraints = [models.CheckConstraint(check=models.Q(puntaje__gte=0.0),name='tv_construccion_puntaje_check')]
        constraints += [models.CheckConstraint(check=models.Q(puntaje__lte=200),name='tv_construccion_puntaje_check1')]
        constraints += [models.CheckConstraint(check=models.Q(valor_metro__gte=0.0),name='tv_construccion_valor_metro_check')]
        constraints += [models.CheckConstraint(check=models.Q(valor_metro__lte=999999999999999),name='tv_construccion_valor_metro_check1')]
        db_table = "tv_construccion"
        managed = False

class Zhg_rural(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,null=True,blank=True,default=uuid.uuid4())
    codigo = models.CharField(max_length=7, null=True,blank=True)
    codigo_zona_geoeconomica = models.CharField(max_length=4,null=True,blank=True)
    valor_hectarea_agropecuario = models.DecimalField(max_digits=16,decimal_places=1,null=True, blank=True)
    valor_hectarea_no_agropecuario = models.DecimalField(max_digits=16,decimal_places=1,null=True, blank=True)
    subzona_fisica = models.CharField(max_length=255,null=True,blank=True)
    vigencia = models.DateField(null=True,blank=True)
    geometria = models.MultiPolygonField(srid=9377, dim=3, null=True,blank=True)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(valor_hectarea_agropecuario__gte=0.0),name='zhg_rural_valor_hectarea_agropecrio_check')]
        constraints += [models.CheckConstraint(check=models.Q(valor_hectarea_agropecuario__lte=999999999999999),name='zhg_rural_valor_hectarea_agropecrio_check1')]
        constraints += [models.CheckConstraint(check=models.Q(valor_hectarea_no_agropecuario__gte=0.0),name='zhg_rural_valor_hectarea_no_grpcrio_check')]
        constraints += [models.CheckConstraint(check=models.Q(valor_hectarea_no_agropecuario__lte=999999999999999),name='zhg_rural_valor_hectarea_no_grpcrio_check1')]
        db_table = "zhg_rural"
        managed = False
    
class Zhg_urbana(models.Model):
    t_id = models.BigAutoField(primary_key=True)
    t_ili_tid = models.CharField(max_length=200,blank=True,null=True,default=uuid.uuid4())
    codigo = models.CharField(max_length=7,null=True,blank=True)
    codigo_zona_geoeconomica = models.CharField(max_length=4,null=True,blank=True)
    valor_metro = models.DecimalField(max_digits=16,decimal_places=1,null=True,blank=True)
    subzona_fisica = models.CharField(max_length=255,null=True,blank=True)
    vigencia = models.DateField(null=True,blank=True)
    geometria = models.MultiPolygonField(srid=9377, dim=3, null=True,blank=True)
    class Meta:
        constraints = [models.CheckConstraint(check=models.Q(valor_metro__gte=0.0),name='zhg_urbana_valor_metro_check')]
        constraints += [models.CheckConstraint(check=models.Q(valor_metro__lte=999999999999999),name='zhg_urbana_valor_metro_check1')]
        db_table = "zhg_urbana"
        managed = False