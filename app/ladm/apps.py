from django.apps import AppConfig


class LadmConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ladm'
