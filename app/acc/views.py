# Python
from vectortiles.postgis.functions import MakeEnvelope, AsMVTGeom
import mercantile
from ast import literal_eval


# Django
from django.contrib.gis.db.models.functions import Transform, Centroid
from django.contrib.gis.geos import Polygon
from django.db import connections
from django.http import HttpResponse

# DRF
from rest_framework.decorators import api_view
from rest_framework.exceptions import NotFound
from rest_framework.viewsets import ModelViewSet

# Vector Tiles
from django.views.generic import ListView
from vectortiles.postgis.views import MVTView

# App
from ladm.models.ladm_acc_v1 import Gc_terreno, Col_uebaunit, Cc_perimetrourbano, Gc_construccion
from .serializers import ParcelSerializer, BuildingSerializer, PerimetrourbanoVistaSerializer
from app.common import MUNICIPIOS_NAMES
from .filters import ParcelFilter
from .functions import responseHttpObj


class ParcelTileView(MVTView, ListView):
    model = Gc_terreno
    vector_tile_layer_name = "features"
    vector_tile_fields = ('codigo', )
    vector_tile_geom_name = 'geometria'
    vector_tile_queryset = Gc_terreno.objects.using('cun25019').all()


def get_tile(x, y, z, vector_tile_queryset, vector_tile_geom_name, vector_tile_fields, vector_tile_layer_name, cod_schema, extent=4096, buffer=256, clip_geom=True):
    # get tile coordinates from x, y and z
    xmin, ymin, xmax, ymax = mercantile.xy_bounds(x, y, z)
    features = vector_tile_queryset
    # keep features intersecting tile
    filters = {
        # GeoFuncMixin implicitly transforms to SRID of geom
        f"{vector_tile_geom_name}__intersects": MakeEnvelope(xmin, ymin, xmax, ymax, 3857)
    }
    features = features.filter(**filters)
    # annotate prepared geometry for MVT
    features = features.annotate(geom_prepared=AsMVTGeom(Transform(vector_tile_geom_name, 3857),
                                                            MakeEnvelope(xmin, ymin, xmax, ymax, 3857),
                                                            extent,
                                                            buffer,
                                                            clip_geom))
    fields = vector_tile_fields + ("geom_prepared", ) if vector_tile_fields else ("geom_prepared", )
    features = features.values(*fields)
    # generate MVT
    sql, params = features.query.get_compiler(cod_schema).as_sql()
    with connections[cod_schema].cursor() as cursor:
        cursor.execute("SELECT ST_ASMVT(subquery.*, %s, %s, %s) FROM ({}) as subquery".format(sql),
                        params=[vector_tile_layer_name, extent, "geom_prepared", *params])
        row = cursor.fetchone()[0]
        return row.tobytes() if row else None

@api_view(["GET"])
def parcel_tiles(request, z, x, y):
    content_type = "application/vnd.mapbox-vector-tile"
    vector_tile_extent = 4096
    vector_tile_buffer = 256
    vector_tile_geom_name = 'geometria'
    vector_tile_fields = ('codigo',)
    vector_tile_layer_name = "parcel"
    cod_schema = 'cun25019'
    vector_tile_queryset = Gc_terreno.objects.using(cod_schema).all()
    content = get_tile(x, y, z, vector_tile_queryset, vector_tile_geom_name, vector_tile_fields, vector_tile_layer_name, cod_schema, extent=vector_tile_extent, buffer=vector_tile_buffer, clip_geom=True)
    status = 200 if content else 204
    return HttpResponse(content, content_type=content_type, status=status)


class ParcelViewSet(ModelViewSet):
    """
     View for data query Parcel LADM
    """
    http_method_names = ['get', 'head', 'options']
    serializer_class = ParcelSerializer
    filterset_class = ParcelFilter
    lookup_field = 'ue_gc_terreno__codigo'

    def get_queryset(self):
        """
        New definition of the queryset to access the related tables Gc_Terreno, Gc_Predio 
        by table Col_uestraunit. 
        In a provisional way, the transformation of coordinates to SRID 4326 is carried out 
        """
        town = self.kwargs.get('town')
        if not town in MUNICIPIOS_NAMES.keys():
            raise NotFound()
        else:
            schema = 'cun' + self.kwargs.get('town')
            queryset = Col_uebaunit.objects.using(schema).select_related('ue_gc_terreno').select_related('baunit').filter(ue_gc_terreno__isnull=False).annotate(geometria=Transform('ue_gc_terreno__geometria', 9377)).all()
            return queryset
    
    def retrieve(self, request, *args, **kwargs):
        town = self.kwargs.get('town')
        if not town in MUNICIPIOS_NAMES.keys():
            return responseHttpObj({"message": 'Municipio no valido'})
        try: 
            instance = self.get_object()
            serializer = self.get_serializer(instance)
            data = {
                "geojson": serializer.data
            }
            return responseHttpObj(data)
        except:
            return responseHttpObj({"message": 'No se encontro el predio'})
           
    def list(self, request, town):
        """
        New definition of the list data to limit the result for layer
        """
        town = self.kwargs.get('town')
        if not town in MUNICIPIOS_NAMES.keys():
            return responseHttpObj({"message": 'Seleccion un Municipio'})
        try:
            bbox = literal_eval(self.request.GET['bbox'])
        except: 
            cod_schema = 'cun' + town 
            query_town = Cc_perimetrourbano.objects.using(cod_schema).filter(tipo_avaluo='01').annotate(point=Centroid('geometria'))
            if(query_town):
                return responseHttpObj(PerimetrourbanoVistaSerializer(query_town[:1], many=True).data)
            else:
                return responseHttpObj({"message": 'Seleccion un Municipio'})
        query_set = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer_class()
        # Check 
        if (not(query_set.exists())):
            return responseHttpObj({"message": 'No se encontro el predio'})
        # Consult Buildings
        schema = 'cun' + town
        queryset_building = Gc_construccion.objects.using(schema).filter(geometria__intersects=Polygon.from_bbox(bbox)).all()[0:100]
        serializer_buildind = BuildingSerializer(queryset_building, many=True)
        # Return lands by Urban Scale
        if (query_set.count() <= 300):
            serializer = self.get_serializer_class()
            serializer = serializer(self.filter_queryset(self.get_queryset()), many=True)
            data =  {
                "parcel": serializer.data,
                "building": serializer_buildind.data
            }
            return responseHttpObj(data)
        else: return responseHttpObj({"message": 'Limite de búsqueda'})
