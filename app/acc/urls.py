# Django
from django.urls import path, include

# Rest Framework
from rest_framework.routers import SimpleRouter

# App
from .views import parcel_tiles, ParcelViewSet, ParcelTileView


router_visor = SimpleRouter()
router_visor.register(r'layer/land', ParcelViewSet, basename='layers/land/')

# The API URLs are now determined automatically by the router.
urlpatterns = [ 
    path('town/<str:town>/', include(router_visor.urls)),
    path('tiles/<int:z>/<int:x>/<int:y>', parcel_tiles, name="parcel-tiles"),
    path('parcel/<int:z>/<int:x>/<int:y>', ParcelTileView.as_view(), name="parcel"),
]