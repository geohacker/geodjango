# Django
from django.contrib.gis.geos import Polygon
import django_filters 

# App
from ladm.models.ladm_acc_v1 import Col_uebaunit

# Class to return the filter set for Predio Vista
class ParcelFilter(django_filters.FilterSet):
    # Filters Attributes
    bbox = django_filters.CharFilter(method='points_bbox')
    def points_bbox(self, queryset, name, value):
        try:
            xmin, ymin, xmax, ymax = value.split(',')
            polygon = Polygon.from_bbox([xmin, ymin, xmax, ymax])
            return queryset.filter(geometria__intersects=polygon)
        except ValueError as exec:
            #There is an error in the point parameter. Failed to filter 
            return queryset.none()
    class Meta:
        model = Col_uebaunit
        fields = ['bbox']
