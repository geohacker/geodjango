from django.http import HttpResponse
import json

# Response of type success content serializers
def responseHttpObj(obj_json_data):
    response = {
        "success": True,
        "data": obj_json_data,
    }
    return HttpResponse(json.dumps(response), content_type='application/json; charset=UTF-8')