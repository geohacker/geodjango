# DRF
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from rest_framework_gis.fields import GeometryField
from rest_framework import serializers

# App
from ladm.models.ladm_acc_v1 import Col_uebaunit, Gc_construccion, Cc_perimetrourbano

class PerimetrourbanoVistaSerializer(GeoFeatureModelSerializer):
    point = GeometryField()

    class Meta:
        model = Cc_perimetrourbano
        geo_field = 'point'
        fields = ['nombre_geografico']

class ParcelSerializer(GeoFeatureModelSerializer):
    geometria = GeometryField()
    codigo = serializers.CharField(source = 'ue_gc_terreno.codigo')
    area_construida = serializers.FloatField(source = 'baunit.area_construida')
    area = serializers.FloatField(source = 'baunit.area')
    nombre = serializers.CharField(source = 'baunit.nombre')
    numero_predial_anterior = serializers.CharField(source = 'baunit.numero_predial_anterior')
    clase_suelo = serializers.CharField(source = 'baunit.clase_suelo.ilicode')
    condicion_predio = serializers.CharField(source = 'baunit.condicion_predio.ilicode')
    destinacion_economica = serializers.CharField(source = 'baunit.destinacion_economica.ilicode')
    matricula_inmobiliaria = serializers.SerializerMethodField()

    def get_matricula_inmobiliaria(self, obj):
        if (obj.baunit.codigo_orip):
            return '{}-{}'.format(obj.baunit.codigo_orip, obj.baunit.matricula_inmobiliaria) 
        else: 
            return obj.baunit.matricula_inmobiliaria
    
    class Meta:
        model = Col_uebaunit 
        geo_field = 'geometria'
        fields = ['codigo', 'area_construida', 'area', 'nombre', 'matricula_inmobiliaria', 'numero_predial_anterior', 'clase_suelo', 'condicion_predio', 'destinacion_economica']


class BuildingSerializer(GeoFeatureModelSerializer):
    
    class Meta:
        model = Gc_construccion 
        geo_field = 'geometria'
        fields = ['codigo']
